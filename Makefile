all: postgres13 postgres12

postgres13: Dockerfile-13
	docker build -t bitraf-postgres:13 -f Dockerfile-13 .
	@if [ "$$CI_REGISTRY_IMAGE" != "" ]; then \
	  docker tag bitraf-postgres:13 $${CI_REGISTRY_IMAGE}:13; \
	  echo "Tagging $${CI_REGISTRY_IMAGE}:13"; \
    fi
